package com.example.myfirtsapi.controllers;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EjemploDeleteController {
    @DeleteMapping("/Eliminar")
    public String ejemplodelete(){
        return "Esto es un delete";
    }
}
